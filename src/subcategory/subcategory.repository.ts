import { NotFoundException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { UpdateSubCategoryDto } from './dto/subcategory-update.dto';
import { SubCategoryDto } from './dto/subcategory.dto';
import { SubCategory } from './subcategory.entity';

@EntityRepository(SubCategory)
export class SubCategoryRepository extends Repository<SubCategory> {
  /**********************
   * create sub category
   *********************/
  async createSubCategory(dto: SubCategoryDto): Promise<SubCategory> {
    const { name, category_id } = dto;

    const subcategory = new SubCategory();

    subcategory.name = name;
    subcategory.category_id = category_id;
    subcategory.created_at = new Date();
    subcategory.modified_at = new Date();

    await subcategory.save();

    return subcategory;
  }

  /*********************
   * Get sub categories
   *********************/
  async getSubCategories(): Promise<SubCategory[]> {
    return await this.find();
  }

  /*************************
   * Get sub category by id
   *************************/
  async getSubCategory(id: number): Promise<SubCategory> {
    const subcategory = await this.findOne(id);

    if (!subcategory) {
      throw new NotFoundException(`No sub category found for id ${id}`);
    }

    return subcategory;
  }

  /******************
   * update category
   ******************/
  async updateSubCategory(
    id: number,
    dto: UpdateSubCategoryDto,
  ): Promise<SubCategory> {
    const subcategory = await this.getSubCategory(id);

    const { name, category_id } = dto;

    if (name) {
      subcategory.name = name;
    }

    if (category_id) {
      subcategory.category_id = category_id;
    }

    subcategory.modified_at = new Date();

    this.update(id, subcategory);

    return subcategory;
  }

  /**********************
   * Delete sub category
   **********************/
  async deleteSubCategory(id: number): Promise<string> {
    try {
      this.delete(id);
    } catch (error) {
      throw Error(error);
    }
    return 'Deleted';
  }
}
