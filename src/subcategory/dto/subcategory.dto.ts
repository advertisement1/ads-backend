import {
  IsInt,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class SubCategoryDto {
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  name: string;

  @IsInt()
  category_id: number;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
