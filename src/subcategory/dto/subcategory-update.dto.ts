import {
  IsInt,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class UpdateSubCategoryDto {
  @IsString()
  @IsOptional()
  @MinLength(6)
  @MaxLength(20)
  name: string;

  @IsInt()
  @IsOptional()
  category_id: number;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
