import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { UpdateSubCategoryDto } from './dto/subcategory-update.dto';
import { SubCategoryDto } from './dto/subcategory.dto';
import { SubCategory } from './subcategory.entity';
import { SubcategoryService } from './subcategory.service';

@Controller('subcategory')
export class SubcategoryController {
  constructor(private service: SubcategoryService) {}

  @Post()
  createSubCategory(@Body() dto: SubCategoryDto): Promise<SubCategory> {
    return this.service.createSubCategory(dto);
  }

  @Get()
  getSubCategories(): Promise<SubCategory[]> {
    return this.service.getSubCategories();
  }

  @Get('/:id')
  getSubCategory(@Param('id') id: number): Promise<SubCategory> {
    return this.service.getSubCategory(id);
  }

  @Patch('/:id')
  updateSubCategory(
    @Param('id') id: number,
    @Body() dto: UpdateSubCategoryDto,
  ): Promise<SubCategory> {
    return this.service.updateCategory(id, dto);
  }

  @Delete('/:id')
  deleteSubCategory(@Param('id') id: number): Promise<string> {
    return this.service.deleteSubCategory(id);
  }
}
