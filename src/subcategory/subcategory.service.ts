import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { UpdateSubCategoryDto } from './dto/subcategory-update.dto';
import { SubCategoryDto } from './dto/subcategory.dto';
import { SubCategory } from './subcategory.entity';
import { SubCategoryRepository } from './subcategory.repository';

@Injectable()
export class SubcategoryService {
  private repository: SubCategoryRepository;

  constructor(private connection: Connection) {
    this.repository = this.connection.getCustomRepository(
      SubCategoryRepository,
    );
  }

  async createSubCategory(dto: SubCategoryDto): Promise<SubCategory> {
    return await this.repository.createSubCategory(dto);
  }

  async getSubCategories(): Promise<SubCategory[]> {
    return await this.repository.getSubCategories();
  }

  async getSubCategory(id: number): Promise<SubCategory> {
    return await this.repository.getSubCategory(id);
  }

  async updateCategory(
    id: number,
    dto: UpdateSubCategoryDto,
  ): Promise<SubCategory> {
    return await this.repository.updateSubCategory(id, dto);
  }

  async deleteSubCategory(id: number): Promise<string> {
    return await this.repository.deleteSubCategory(id);
  }
}
