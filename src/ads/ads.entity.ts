import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

@Entity()
@Unique(['title'])
export class Ads extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  cover: string;

  @Column('decimal', { precision: 10, scale: 2 })
  original_price: number;

  @Column('decimal', { precision: 10, scale: 2 })
  discount_rate: number;

  @Column()
  category_id: number;

  @Column()
  sub_category_id: number;

  @Column()
  publisher_id: number;

  @Column()
  created_at: Date;

  @Column()
  modified_at: Date;
}
