import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Ads } from './ads.entity';
import { AdsService } from './ads.service';
import { AdsReadDto } from './dto/ads-read.dto';
import { AdsUpdateDto } from './dto/ads-update.dto';
import { AdsDto } from './dto/ads.dto';

@Controller('advertisement')
export class AdsController {
  constructor(private service: AdsService) {}

  @Post()
  createAds(@Body() dto: AdsDto): Promise<AdsReadDto> {
    return this.service.createAds(dto);
  }

  @Get()
  getAds(): Promise<AdsReadDto[]> {
    return this.service.getAds();
  }

  @Get('/:id')
  getAd(@Param('id') id: number): Promise<AdsReadDto> {
    return this.service.getAd(id);
  }

  @Patch('/:id')
  updateAds(@Param('id') id: number, dto: AdsUpdateDto): Promise<AdsReadDto> {
    return this.service.updateAds(id, dto);
  }

  @Delete('/:id')
  deleteAds(@Param('id') id: number): Promise<string> {
    return this.service.deleteAds(id);
  }
}
