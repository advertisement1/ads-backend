import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { Ads } from './ads.entity';
import { AdsRepository } from './ads.repository';
import { AdsReadDto } from './dto/ads-read.dto';
import { AdsUpdateDto } from './dto/ads-update.dto';
import { AdsDto } from './dto/ads.dto';

@Injectable()
export class AdsService {
  private repository: AdsRepository;

  constructor(private connection: Connection) {
    this.repository = this.connection.getCustomRepository(AdsRepository);
  }

  async createAds(dto: AdsDto): Promise<AdsReadDto> {
    return await this.repository.createAds(dto);
  }

  async getAds(): Promise<AdsReadDto[]> {
    return await this.repository.getAds();
  }

  async getAd(id: number): Promise<AdsReadDto> {
    return await this.repository.getAd(id);
  }

  async updateAds(id: number, dto: AdsUpdateDto): Promise<AdsReadDto> {
    return await this.repository.updateAds(id, dto);
  }

  async deleteAds(id: number): Promise<string> {
    return await this.repository.deleteAds(id);
  }
}
