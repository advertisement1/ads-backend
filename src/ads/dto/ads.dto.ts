import {
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

import { AdsPrice } from './models/ads-price.model';
import { AdsCategory } from './models/ads-category.model';

export class AdsDto {
  @IsString()
  @MinLength(10)
  @MaxLength(40)
  title: string;

  price: AdsPrice;

  category: AdsCategory;

  @IsInt()
  publisher_id: number;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
