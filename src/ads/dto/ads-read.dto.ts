import {
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

import { AdsPrice } from './models/ads-price.model';
import { AdsCategory } from './models/ads-category.model';

export class AdsReadDto {
  @IsString()
  @MinLength(10)
  @MaxLength(40)
  title: string;

  @IsString()
  cover: string;

  price: AdsPrice;

  category: AdsCategory;

  @IsInt()
  publisher_id: number;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
