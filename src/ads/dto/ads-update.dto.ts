import {
  IsInt,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

import { AdsPrice } from './models/ads-price.model';
import { AdsCategory } from './models/ads-category.model';

export class AdsUpdateDto {
  @IsString()
  @IsOptional()
  @MinLength(10)
  @MaxLength(40)
  title: string;

  @IsOptional()
  price: AdsPrice;

  @IsOptional()
  category: AdsCategory;

  @IsInt()
  @IsOptional()
  publisher_id: number;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
