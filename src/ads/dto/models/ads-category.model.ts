import { IsNumber } from 'class-validator';

export class AdsCategory {
  @IsNumber()
  category: number;

  @IsNumber()
  sub_category: number;
}
