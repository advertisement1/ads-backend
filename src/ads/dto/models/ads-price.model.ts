import { IsDecimal, IsNumber } from 'class-validator';

export class AdsPrice {
  @IsDecimal()
  original: number;

  @IsDecimal()
  discount_rate: number;
}
