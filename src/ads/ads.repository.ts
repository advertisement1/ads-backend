import { NotFoundException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Ads } from './ads.entity';
import { AdsReadDto } from './dto/ads-read.dto';
import { AdsUpdateDto } from './dto/ads-update.dto';
import { AdsDto } from './dto/ads.dto';

@EntityRepository(Ads)
export class AdsRepository extends Repository<Ads> {
  convertToReadDto(ads: Ads) {
    return {
      title: ads.title,
      cover: ads.cover,
      price: {
        original: ads.original_price,
        discount_rate: ads.discount_rate,
      },
      category: {
        category: ads.category_id,
        sub_category: ads.sub_category_id,
      },
      publisher_id: ads.publisher_id,
      created_at: ads.created_at,
      modified_at: ads.modified_at,
    };
  }

  /***********************
   * create advertisement
   ***********************/
  async createAds(dto: AdsDto): Promise<AdsReadDto> {
    const { title, price, category, publisher_id } = dto;

    const ads = new Ads();

    ads.title = title;
    ads.cover = '';
    ads.original_price = price.original;
    ads.discount_rate = price.discount_rate;
    ads.category_id = category.category;
    ads.sub_category_id = category.sub_category;
    ads.publisher_id = publisher_id;
    ads.created_at = new Date();
    ads.modified_at = new Date();

    ads.save();

    return this.convertToReadDto(ads);
  }

  /*********************
   * Get advertisements
   *********************/
  async getAds(): Promise<AdsReadDto[]> {
    const results = await this.find();

    return results.map((result) => this.convertToReadDto(result));
  }

  /**************************
   * Get advertisement by id
   **************************/
  async getAd(id: number): Promise<AdsReadDto> {
    const result = await this.findOne(id);

    if (!result) {
      throw new NotFoundException(`No advertisement found`);
    }

    return this.convertToReadDto(result);
  }

  /***********************
   * update advertisement
   ***********************/
  async updateAds(id: number, dto: AdsUpdateDto): Promise<AdsReadDto> {
    const result = await this.findOne(id);

    const { title, price, category, publisher_id } = dto;

    if (title) {
      result.title = title;
    }

    if (price.original) {
      result.original_price = price.original;
    }

    if (price.discount_rate) {
      result.discount_rate = price.discount_rate;
    }

    if (category.category) {
      result.category_id = category.category;
    }

    if (category.sub_category) {
      result.sub_category_id = category.sub_category;
    }

    if (publisher_id) {
      result.publisher_id = publisher_id;
    }

    result.modified_at = new Date();

    this.update(id, result);

    return this.convertToReadDto(result);
  }

  /***********************
   * delete advertisement
   ***********************/
  async deleteAds(id: number): Promise<string> {
    this.delete(id);
    return 'Deleted';
  }
}
