import { IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class UpdateCategoryDto {
  @IsString()
  @IsOptional()
  @MinLength(6)
  @MaxLength(20)
  name: string;

  @IsString()
  @IsOptional()
  @MinLength(15)
  description: string;

  @IsString()
  @IsOptional()
  icon: string;
}
