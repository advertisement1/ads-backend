import { IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class CategoryDto {
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  name: string;

  @IsString()
  @MinLength(15)
  description: string;

  @IsString()
  icon: string;

  @IsOptional()
  created_at: Date;

  @IsOptional()
  modified_at: Date;
}
