import { NotFoundException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Category } from './category.entity';
import { UpdateCategoryDto } from './dto/category-update.dto';
import { CategoryDto } from './dto/category.dto';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  /*****************
   * Create category
   *****************/
  async createCategory(
    dto: CategoryDto,
    file: Express.Multer.File,
  ): Promise<Category> {
    const { name, description, icon } = dto;

    const category = new Category();

    category.name = name;
    category.description = description;
    category.icon = icon;
    category.cover = file.filename;
    category.created_at = new Date();
    category.modified_at = new Date();

    await category.save();

    return category;
  }

  /********************
   * Get all categories
   ********************/
  async getCategories(): Promise<Category[]> {
    return await this.find();
  }

  /********************
   * Get category by id
   ********************/
  async getCategory(id: number): Promise<Category> {
    const category = await this.findOne(id);

    if (!category) {
      throw new NotFoundException(`No category found for the id ${id}`);
    }

    return category;
  }

  /*****************
   * Update category
   *****************/
  async updateCategory(id: number, dto: UpdateCategoryDto): Promise<Category> {
    const category = await this.getCategory(id);

    const { name, description, icon } = dto;

    if (name) {
      category.name = name;
    }

    if (description) {
      category.description = description;
    }

    if (icon) {
      category.icon = icon;
    }

    category.modified_at = new Date();

    await this.update(id, category);

    return category;
  }

  /*********************
   * Change cover image
   *********************/
  async changeCover(id: number, file: Express.Multer.File): Promise<Category> {
    const category = await this.getCategory(id);

    category.cover = file.filename;
    category.modified_at = new Date();

    this.update(id, category);

    return category;
  }

  /******************
   * Delete category
   ******************/
  async deleteCategory(id: number): Promise<string> {
    await this.delete(id);
    return 'Deleted';
  }
}
