import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { Category } from './category.entity';
import { CategoryRepository } from './category.repository';
import { UpdateCategoryDto } from './dto/category-update.dto';
import { CategoryDto } from './dto/category.dto';

@Injectable()
export class CategoryService {
  private repository: CategoryRepository;

  constructor(private connection: Connection) {
    this.repository = this.connection.getCustomRepository(CategoryRepository);
  }

  async create(dto: CategoryDto, file: Express.Multer.File): Promise<Category> {
    return await this.repository.createCategory(dto, file);
  }

  async getAll(): Promise<Category[]> {
    return await this.repository.getCategories();
  }

  async getById(id: number): Promise<Category> {
    return await this.repository.getCategory(id);
  }

  async updateById(id: number, dto: UpdateCategoryDto): Promise<Category> {
    return this.repository.updateCategory(id, dto);
  }

  async changeCover(id: number, file: Express.Multer.File): Promise<Category> {
    return this.repository.changeCover(id, file);
  }

  async deleteCategory(id: number): Promise<string> {
    return this.repository.deleteCategory(id);
  }
}
