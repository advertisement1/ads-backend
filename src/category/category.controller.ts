import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { Category } from './category.entity';
import { CategoryService } from './category.service';
import { UpdateCategoryDto } from './dto/category-update.dto';
import { CategoryDto } from './dto/category.dto';

@Controller('category')
@UseGuards(AuthGuard())
export class CategoryController {
  constructor(private service: CategoryService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  create(
    @Body() dto: CategoryDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Category> {
    return this.service.create(dto, file);
  }

  @Get()
  getAll(): Promise<Category[]> {
    return this.service.getAll();
  }

  @Get('/:id')
  getById(@Param('id') id: number): Promise<Category> {
    return this.service.getById(id);
  }

  @Patch('/:id')
  updateById(
    @Param('id') id: number,
    @Body(new ValidationPipe({ transform: true })) dto: UpdateCategoryDto,
  ): Promise<Category> {
    console.log('ID', id);
    console.log('DTO', dto);
    return this.service.updateById(id, dto);
  }

  @Patch('/cover/:id')
  @UseInterceptors(FileInterceptor('file'))
  changeCover(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Category> {
    return this.service.changeCover(id, file);
  }

  @Delete('/:id')
  deleteCategory(@Param('id') id: number): Promise<string> {
    return this.service.deleteCategory(id);
  }
}
